﻿using Xunit;
using Vivan.Extensions.StringExtensions;

namespace Vivan.Extensions.Tests
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData("MINHA STRING TUNADONA", "Minha String Tunadona")]
        [InlineData("minha string tunadona", "Minha String Tunadona")]
        [InlineData("mINHA sTRING tUNADONA", "Minha String Tunadona")]
        [InlineData("Minha string tunadona", "Minha String Tunadona")]
        public void ToTitleCaseFoiConvertidoCorretamente(string stringNaoEditada, string resultadoEsperado)
        {
            string stringEditada = stringNaoEditada.ToTitleCase();

            Assert.Equal(stringEditada, resultadoEsperado);
        }
    }
}
