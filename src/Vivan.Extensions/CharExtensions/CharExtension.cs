﻿namespace Vivan.Extensions.CharExtensions
{
    public static class CharExtension
    {
        public static bool SNToBool(this char value)
        {
            return value.ToString().ToUpper() == "S" ? true : false;
        }

        public static bool AIToBool(this char value)
        {
            return value.ToString().ToUpper() == "A" ? true : false;
        }
    }
}
