﻿using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

namespace Vivan.Extensions.Enums
{
    public static class EnumExtension
    {
        /// <summary>
        /// Retorna o valor definido para o atributo 'DisplayName' do enum
        /// </summary>
        /// <returns>String - DisplayName do atributo do enum</returns>
        public static string GetDisplayName(this Enum enumValue)
        {
            if (enumValue == null)
                return string.Empty;

            return enumValue.GetType()?
                            .GetMember(enumValue.ToString())?
                            .First()?
                            .GetCustomAttribute<DisplayNameAttribute>()?
                            .DisplayName;
        }
    }
}
