﻿using System.Collections.Generic;
using System.Linq;

namespace Vivan.Extensions.IEnumerables
{
    public static class IEnumerableExtension
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }
    }
}
