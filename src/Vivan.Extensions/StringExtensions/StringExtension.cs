﻿using Humanizer;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Vivan.Extensions.StringExtensions
{
    public static class StringExtension
    {
        public static string WithMaxLength(this string value, int maxLength)
        {
            return value?.Substring(0, Math.Min(value.Length, maxLength));
        }

        public static string ToOnlyNumbers(this string value)
        {
            string OnlyNumbersPattern = @"[^\d]";

            return Regex.Replace(value, OnlyNumbersPattern, string.Empty);
        }

        public static string RemoveAccents(this string value)
        {
            string normalized = value.Normalize(NormalizationForm.FormKD);
            Encoding removal = Encoding.GetEncoding(Encoding.ASCII.CodePage,
                                                    new EncoderReplacementFallback(""),
                                                    new DecoderReplacementFallback(""));
            byte[] bytes = removal.GetBytes(normalized);
            return Encoding.ASCII.GetString(bytes);
        }

        public static bool SNToBool(this string value)
        {
            return value?.ToUpper() == "S" ? true : false;
        }

        public static bool AIToBool(this string value)
        {
            return value?.ToUpper() == "A" ? true : false;
        }

        public static string ToTitleCase(this string value)
        {
            return value.ToLower().Transform(To.TitleCase);
        }
    }
}
