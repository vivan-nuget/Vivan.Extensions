﻿using System.Collections.Generic;
using System.Linq;

namespace Vivan.Extensions.IReadOnlyCollections
{
    public static class IReadOnlyCollectionExtension
    {
        public static bool IsNullOrEmpty<T>(this IReadOnlyCollection<T> collection)
        {
            return collection == null || !collection.Any();
        }
    }
}
