﻿namespace Vivan.Extensions.Booleans
{
    public static class BooleanExtension
    {
        public static int ToBinary(this bool? value)
        {
            if (!value.HasValue)
                return false.ToBinary();

            return value.Value.ToBinary();
        }

        public static int ToBinary(this bool value)
        {
            return value ? 1 : 0;
        }

        public static char ToAI(this bool? value)
        {
            if (!value.HasValue)
                return false.ToAI();

            return value.Value.ToAI();
        }

        public static char ToAI(this bool value)
        {
            return value ? 'A' : 'I';
        }

        public static char ToSN(this bool? value)
        {
            if (!value.HasValue)
                return false.ToSN();

            return value.Value.ToSN();
        }

        public static char ToSN(this bool value)
        {
            return value ? 'S' : 'N';
        }

        public static string ToSimNao(this bool? value)
        {
            if (!value.HasValue)
                return false.ToSimNao();

            return value.Value.ToSimNao();
        }

        public static string ToSimNao(this bool value)
        {
            return value ? "Sim" : "Não";
        }
    }
}
