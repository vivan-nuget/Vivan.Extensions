# Vivan.Extensions

Biblioteca que possui um conjunto de métodos de extensões para tipos de dados e objetos como DateTime, ICollection, bool e etc...

## Instalação

Instalar o package **'Vivan.Extensions'** através do Nuget Package Manager (Servidor da Vivan).

## Dependências

## Recursos
* BooleanExtension
* CharExtension
* DateTimeExtension
* DecimalExtension
* EnumExtension
* ICollectionExtension
* IEnumerableExtension
* IQueryableExtensions
* IReadOnlyCollectionExtension
* StringExtension

## Utilizando os recursos da classe BooleanExtension

A classe **BooleanExtension** fornece as seguintes extensões:

```c#
//Retorna 1 para true e 0 para false
int ToBinary(this bool? value); 
int ToBinary(this bool value);

//Retorna 'A' para true e 'I' para false
char ToAI(this bool? value);
char ToAI(this bool value);

//Retorna 'S' para true e 'N' para false
char ToSN(this bool? value);
char ToSN(this bool value);

//Retorna "Sim" para true e "Não" para false
string ToSimNao(this bool? value);
string ToSimNao(this bool value);
```

## Utilizando os recursos da classe DateTimeExtension

A classe **DateTimeExtension** fornece as seguintes extensões:

```c#
//Retorna se a data é diferente de default
bool IsNotDefault(this DateTime date);

//Retorna o primeiro dia da semana
DateTime FirstDayOfWeek(this DateTime dt);

//Retorna o último dia da semana
DateTime LastDayOfWeek(this DateTime dt);

//Retorna o primeiro dia do mês
DateTime FirstDayOfMonth(this DateTime dt);

//Retorna o último dia do mês
DateTime LastDayOfMonth(this DateTime dt);

//Retorna o primeiro dia do mês seguinte
DateTime FirstDayOfNextMonth(this DateTime dt);
```

## Utilizando os recursos da classe EnumExtension

A classe **EnumExtension** fornece as seguintes extensões:

```c#
//Retorna o valor definido para o atributo 'DisplayName' do enum
string GetDisplayName(this Enum enumValue)
```

## Utilizando os recursos da classe ICollectionExtension


A classe **ICollectionExtension** fornece as seguintes extensões:

```c#
//Retorna true quando a lista for nula ou vazia
bool IsNullOrEmpty<T>(this ICollection<T> collection)

//Exemplo:
List<int> a;

if (a.IsNullOrEmpty())
//Do Something
```

## Utilizando os recursos da classe StringExtension

A classe **StringExtension** fornece as seguintes extensões:

```c#
//Trunca string para a quantidade de caracteres informada
string WithMaxLength(this string value, int maxLength);

//Remove todos os caracteres que não são números de uma string
string ToOnlyNumbers(this string value);

// Remove acentos e caracteres especiais da string
string RemoveAccents(this string value)

// Converte string S(Sim) e N(Não) para seus respectivos booleans, S = true, N ou outra coisa = false
bool SNToBool(this string value)

// Converte string A(Ativo) e I(Inativo) para seus respectivos booleans, A = true, I ou outra coisa = false
bool AIToBool(this string value)

// Converte string para formato de título. Exemplo:  "MINHA STRING TUNADONA".ToTitleCase() = "Minha String Tunadona"
string ToTitleCase(this string value)
```

